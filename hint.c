#define NUMBER_OF_QUESTIONS 11
#define MAXIMUM_STUDENTS_IN_GROUP 15
#define STUDENT_ID_SIZE 5

typedef struct {
	char student_id[ STUDENT_ID_SIZE + 1 ];
	char student_answers[ NUMBER_OF_QUESTIONS + 1 ];
	int right, wrong, grade;		// # right, # wrong, grade out of 100.
} student_response_t;

typedef struct {
	char group[ 3 ];
	int number_of_students;
	student_response_t r[ MAXIMUM_STUDENTS_IN_GROUP ];
} group_t; 




int main()
{
    .
    .
    .
	FILE *INFILE = fopen( "group_results.txt", "r" );
	if ( ! INFILE ) {
		fprintf( stderr, "Can't find input file group_results.txt.");
		exit( EXIT_FAILURE );
	}
	read_complete_group( &INFILE, &group );
	fclose( INFILE );
    .
    .
    .
	return EXIT_SUCCESS;
}




void read_complete_group( FILE **in_fileptr, group_t *g )
{
	FILE *IN = *in_fileptr;

	fscanf( IN, "%s", g->group );
	fscanf( IN, "%d", &g->number_of_students );
	printf( "Processing group %s with %d students.\n", g->group, g->number_of_students );

	int i;
	for ( i = 0; i < g->number_of_students; i++ ) {
		fscanf( IN, "%5s", g->r[ i ].student_id );
		g->r[ i ].student_id[ STUDENT_ID_SIZE ] = '\0';
		printf( "Read in THIS student id %s.\n", g->r[ i ].student_id );
		fgets( g->r[ i ].student_answers, NUMBER_OF_QUESTIONS, IN );
		printf( "Read in the list of questions: %s.\n", g->r[ i ].student_answers );
		g->r[ i ].student_answers[ NUMBER_OF_QUESTIONS ] = '\0';
		printf( "Read in the list of questions: %s.\n", g->r[ i ].student_answers );
		fgetc( IN );
	}
	return;
}
